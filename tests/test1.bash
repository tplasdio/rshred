#!/usr/bin/bash

dirlevels=(
 "{a1,a2,a3}"
 "{b1,b2}"
 "{c1,c2}"
)

filelevels=(
 "{f1,f2}"
 "{g1,g2,g3}"
 "{h1,h2}"
)

IFS=/ eval \
	tree='"${dirlevels[*]}"' \
	leaves='"${dirlevels[*]}"/"${filelevels[-1]}"'

shopt -s globstar

unset IFS

count_total_tests=0
count_success_tests=0

d=show_test_info
show_test_info()
{
	printf '\033[1;34m==================================================\033[m\n'
	printf 'Testing: \033[1;33m%s\033[m\n' "$1"
	printf '\033[1;34m==================================================\033[m\n'
	"$@"
	es=$?
	if [ "$es" -eq 0 ]; then
		printf '\033[1;34m= \033[1;32mSuccess!\033[1;34m========================================\033[m\n'
		: "$((count_success_tests += 1))"
	else
		printf '\033[1;34m= \033[1;31mFailure! $?: %s\033[34m =================================\033[m\n' "$es"
	fi
	: "$((count_total_tests += 1))"
	echo
}

function print_test_summary
{
	printf "Summary: $count_success_tests/$count_total_tests tests succeded\n";
}

function mktree
{
	eval mkdir -p -- $tree
}

function mkleaves
{
	printf -- "$1" | eval tee $leaves > /dev/null
}

c=rmtree
function rmtree
{
	rm -rf -- *
	"$@"
}

function test_shred_empty_leaves
{
	# Shred doesn't fill random data on empty files
	# TODO: should rshred force this?
	mktree
	mkleaves
	"$exe_path" -fy

	for leave in **/h*; do
		[[ -s "$leave" ]] || return 1
	done
}

function test_shred_nonempty_leaves
{
	local extraflag="$1"

	mktree
	mkleaves "x"
	"$exe_path" -fy $extraflag

	# Need to wait till files stop shredding
	# TODO: option to wait till is finished shredding
	sleep 2

	# Check if every leaf was shredded, else fail
	for leave in **/h*; do
		[[ "$(< "$leave")" != "x" ]] &>/dev/null || return 1
		[[ "$(du -b "$leave" | cut -f1)" -eq 4096 ]] || return 2
	done
}

function test_shred_rm_nonempty_leaves
{
	test_shred_nonempty_leaves -u 2>/dev/null
	[[ "$(find . ! -path . -type f | wc -l)" -eq 0 ]]
}

function test_shred_directories
{
	mktree

	local count_directories=$(find . ! -path . -type d | wc -l)

	"$exe_path" -dy

	readarray -t filenames < <(find . ! -path . -type d -printf '%f\n')

	# Check if every directory name is just a number
	for filename in "${filenames[@]}"; do
		[[ "$filename" != *[![:digit:]]* ]] || return 1
	done

	# Check if any directories were removed/overwritten
	[[ "${#filenames[@]}" -eq "$count_directories" ]]

}

function test_shred_rm_directories
{
	mktree

	# TODO: catch mv warnings on no longer existent directories as errors
	"$exe_path" -dyu

	[[ "$(find . ! -path . | wc -l)" -eq 0 ]]
}

function die { >&2 printf -- "$@"; exit "$1"; }

function mktestdir
{
	local name="${1:-testdir}" exists isdir

	[ ! -e "$name" ]; exists=$?
	[ ! -d "$name" ]; isdir=$?

	case "$((2#${isdir}${exists}))" in
	(0);;
	(1) rm -rf -- "$name" || die 10 "could not remove old file: $name";;
	(*) return
	esac
	mkdir "$name" || die 11 "could not create testdirectory: $name"

}

function main
{

	case "$1" in
	(-v|-x) set -x
	esac

	rootdir=$(git rev-parse --show-toplevel) \
	exe_path="$rootdir/rshred"

	cd -- "$rootdir"
	[ -x "$exe_path" ] || chmod u+x -- "$exe_path"
	cd -- "$OLDPWD"

	mktestdir && cd testdir || return $?
	trap 'rm -rf -- *' INT EXIT

	$c $d test_shred_nonempty_leaves
	$c $d test_shred_rm_nonempty_leaves
	$c $d test_shred_directories
	$c $d test_shred_rm_directories

	print_test_summary
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
	main "$@" || exit $?
fi
