<p align="center">
  <h1 align="center">Rshred</h1>
  <p align="center">
    A recursive shred command
  </p>
</p>


## 📰 Description

It is a wrapper around the `shred` utility that operates on files and
directories under the directories passed as arguments. Shredding a
directory simply means renaming it to a correlative number.

⚠️ **Do not use on flash memory (SSDs, SD cards) or on any hard drive that
uses techniques like wear leveling** ⚠️. Nor `shred` nor any other
tool that overwrites filesystem blocks on hard drives can reliably do it
on flash memory for several reasons, they just decrease the lifetime
of these devices. See links [below](#see-also). Otherwise, if for
some reason unrelated to preventing file recovery you still want to
use this, feel free.

## 🚀 Installation

<details>
<summary>📦 From source</summary>

- *Dependencies*:
  - `bash`
  - `shred`
  - GNU `find`, or a version that has the `-print0` option
  - GNU `sort`, or a version that has the `-z` option

- *Build Dependencies*:
  - `sed`
  - [`getoptions`](https://github.com/ko1nksm/getoptions), or optionally install it later as runtime dependency

```sh
git clone --filter=blob:none https://codeberg.org/tplasdio/rshred.git
cd rshred
./make.sh install  # or
# make install
```

Run the `make.sh` as root for system-installation.

For uninstallation, run either `./make.sh uninstall` or `make uninstall`.

</details>

<details>
<summary>Arch Linux or other pacman distros</summary>

```sh
curl -O https://codeberg.org/tplasdio/rshred/raw/branch/main/packaging/PKGBUILD-git
PACMAN=yay makepkg -sip PKGBUILD-git   # ← or your AUR helper
```

For uninstallation, run `sudo pacman -Rsn rshred-git`.

</details>

## ⭐ Usage

Due to the ⚠️ **extremely dangerous** ⚠️ nature of this command, a vibrant
 confirmation prompt is shown by default and a long text has to be entered.
I **strongly** advice to first do some practice runs on dummy test
 directory trees to see if the script would run correctly. Use it
consciously, data is valuable.

```sh
rshred                  # Shred files and directories under current one, confirmation prompt
rshred -f               # Shred files under current directory
rshred -d               # Shred directories under current directory
rshred -u               # Shred files and directories, and remove them afterwards
rshred -y               # Shred files and directories under current one, no confirmation (dangerous!!)
rshred dir1 dir2        # Shred files and directories under these directories
rshred -fduy dir1 dir2  # Shred and remove everything under these directories
rshred -f -A-z          # Pass any other shred argument like `-z`
rshred --help           # Show help
```
## 👀 See also
- [Wiping of flash memory](https://wiki.archlinux.org/title/Securely_wipe_disk#Flash_memory)
- [SSD cell clearing](https://wiki.archlinux.org/title/Solid_state_drive/Memory_cell_clearing)
- [Wear leveling](https://en.wikipedia.org/wiki/Wear_leveling)
- [StackExchange answer](https://unix.stackexchange.com/questions/593181/is-shred-bad-for-erasing-ssds)

## 📝 License
GPL-3.0-or-later.
