#!/bin/sh
setup()
{
	: \
	"${DESTDIR:="/"}" \
	"${PRG:=rshred}" \
	"${XDG_PREFIX:="${HOME}/.local"}"

	PRG_PATH=rshred \
	LICENSE=LICENSE \
	PARSER_PATH=parser.sh

	[ "$(id -u)" -eq 0 ] || [ "$FORCEROOT" = 1 ] \
		&& isroot=: \
		|| isroot=false

	$isroot \
		&& prefix="${PREFIX:=/usr/local}" \
		|| prefix="${XDG_PREFIX}"

	bindir="$prefix/bin" \
	datadir="$prefix/share/rshred" \
	licensedir="$prefix/share/licenses/rshred"

	hash getoptions >/dev/null 2>&1 \
		&& hasgetoptions=: \
		|| hasgetoptions=false
}

build()
{
	mkdir -p -- build

	if $hasgetoptions; then
		#embed generated code: getoptions as build dep
		gengetoptions embed "${PRG_PATH}" > "build/$PRG"
	else
		#embed original code: getoptions as runtime dep
		while IFS= read -r line; do
			case $line in
			("# @gengetoptions"*)
				printf -- "%s\n" "$line"
				cat -- "${PARSER_PATH}"
				echo 'eval "$(getoptions parse parse) exit 1"'
			;;
			(*) printf -- "%s\n" "$line"
			esac
		done <  "${PRG_PATH}" > "build/$PRG"
	fi

	printf -- "%s\n" \
		"$(sed '/@make-datadir/s|/usr/share/rshred|'"${datadir}"'|' "build/$PRG")" > "build/$PRG"

}

clean()
{
	rm -rf -- build/*
}

install()
{	build
	mkdir -p -- "${DESTDIR}/${bindir}" "${DESTDIR}/${datadir}" "${DESTDIR}/${licensedir}"

	chmod 644 -- lib/ble_array.bash "${LICENSE}"
	cp -- lib/ble_array.bash "${DESTDIR}${datadir}"

	chmod 755 -- "build/${PRG}"
	cp -- "build/${PRG}" "${DESTDIR}${bindir}/${PRG}"

	cp -- "$LICENSE" "${DESTDIR}/${licensedir}/LICENSE"
}

uninstall()
{
	rm -rf -- "${DESTDIR}${licensedir}"
	rm -f  -- "${DESTDIR}${bindir}/${PRG}"
	rm -rf -- "${DESTDIR}${datadir}"
}

main()
{
	set -x
	setup || return $?
	target="${1-install}"

	case "$target" in
	(i|install)   install;;
	(u|uninstall) uninstall;;
	(b|build)     build;;
	(c|clean)     clean;;
	(*)
		>&2 printf -- 'Unknown target given: "%s"\n' "$1"
		return 22
	esac
}

main "$@" || exit $?
