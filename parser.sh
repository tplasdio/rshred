# @getoptions
parse() {
	setup REST
	flag Opt_f -f --fics
	flag Opt_d -d --dirs
	flag Opt_u -u --remove
	flag Opt_y -y --yes
	param :"push_arg" -A --shred-args init:'shred_args=()'
	disp VERSION -V --version
	disp :usage -h --help
}
# @end
