#!/usr/bin/make -f

VARS = \
	PRG='$(PRG)' \
	DESTDIR='$(DESTDIR)' \
	PREFIX='$(PREFIX)' \
	FORCEROOT='$(FORCEROOT)'

install:
	$(VARS) ./make.sh install

uninstall:
	$(VARS) ./make.sh uninstall

build:
	$(VARS) ./make.sh build

clean:
	$(VARS) ./make.sh clean

test:
	tests/test1.bash

.PHONY: install uninstall build clean
